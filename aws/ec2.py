import boto3  
from botocore.config import Config

class AWS_EC2:
    def connect_ec2():
        #Instances information
        #using client object  
        Myec2_cli=boto3.client('ec2')
        response_cli=Myec2_cli.describe_instances()
        Reservations = response_cli['Reservations'] 
        ResponseMetadata = response_cli['ResponseMetadata']
        return Reservations, ResponseMetadata

