from rest_framework import viewsets, parsers
from .models import  Bucket
from .serializers import  BucketSerializer, EC2Serializer

from .s3 import AWS_S3
from .models import Bucket, EC2


from .save_to_database import SaveToDB
from django.db import connection



# Test for existence of tables in db  "i.e, IS makemigrations done before run these code"  
db_tables = connection.introspection.table_names()
save_my_aws = SaveToDB()

if 'aws_bucket' in db_tables:
    save_my_aws.save_s3_buckets()
if  'aws_ec2' in db_tables:
    save_my_aws.save_ec2_instances()


class BucketViewset(viewsets.ModelViewSet):
    queryset = Bucket.objects.all()
    serializer_class = BucketSerializer
    parser_classes = [parsers.MultiPartParser, parsers.FormParser]
    http_method_names = ['get', 'post', 'patch', 'delete']



class EC2Viewset(viewsets.ModelViewSet):
    queryset = EC2.objects.all()
    serializer_class = EC2Serializer
    parser_classes = [parsers.MultiPartParser, parsers.FormParser]
    http_method_names = ['get', 'post', 'patch', 'delete']
