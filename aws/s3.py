from libcloud.storage.types import Provider
from libcloud.storage.providers import get_driver
import boto3
from datetime import datetime, timedelta



class AWS_S3:
    """
    AWS_S3 : works on all buckets level ,
    assumes that aws credentials are provided by aws cli ,
    covers buckets names, access permissions, privacy and size .
    """

    # class attribute
    s3_resource = boto3.resource('s3')
    s3_client = boto3.client('s3')
    buckets_names = [bucket.name for bucket in s3_resource.buckets.all()]

    def buckets_list(self):
        # buckets_names = [bucket.name for bucket in self.s3_resource.buckets.all()]
        return self.buckets_names


    def access_permissions(self):
        AccessPermissions = {}
        for bucket in self.buckets_names:
            access_control_list = self.s3_client.get_bucket_acl(Bucket=bucket)
            Permission_List = []
            for Permission in access_control_list:
                Permission_List.append(Permission)

            AccessPermissions[bucket] = Permission_List
        return AccessPermissions


    def bucket_privacy(self):
        Public_OR_Private = {}
        for bucket in self.buckets_names:
            response = self.s3_client.get_public_access_block(Bucket=bucket)
            if response['PublicAccessBlockConfiguration']['BlockPublicAcls'] and response['PublicAccessBlockConfiguration']['BlockPublicPolicy'] :
                privacy = "Private"
            else:
                privacy = "Public"

            Public_OR_Private[bucket] = privacy

        return Public_OR_Private

    def buckets_size(self):
        buckets_size = {}
        buckets_objects = [self.s3_resource.Bucket(bucket) for bucket in self.buckets_names]
        total_size = 0
        for buc_obj , buc_name in zip(buckets_objects, self.buckets_names):
            for k in buc_obj.objects.all():
                total_size += k.size
            buckets_size[buc_name] = total_size
        return buckets_size

    def __len__(self):
        return len(self.buckets_names)