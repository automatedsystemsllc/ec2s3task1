from .s3 import AWS_S3
from .ec2 import AWS_EC2
from .models import Bucket, EC2



class SaveToDB:
	def __init__(self):
		pass


	def save_s3_buckets(self):
		# Store s3 buckets objects in db
		my_aws_s3 = AWS_S3()
		buckets_list = my_aws_s3.buckets_list()
		access_permissions = my_aws_s3.access_permissions()
		bucket_privacy = my_aws_s3.bucket_privacy()
		buckets_size = my_aws_s3.buckets_size()
		buckets_no = len(my_aws_s3)

		if buckets_no == 1:
		    bucket = Bucket(
		        bucket_name=buckets_list,
		        access_permissions = access_permissions,
		        bucket_privacy = bucket_privacy,
		        bucket_size = buckets_size,
		    )
		    if not Bucket.objects.filter(bucket_name=buckets_list).exists(): 
		        bucket.save()
		        
		elif buckets_no != 1:
		    for i in range(0, buckets_no+1):
		        bucket = Bucket(
		            bucket_name=buckets_list[i],
		            access_permissions = access_permissions[i],
		            bucket_privacy = bucket_privacy[i],
		            bucket_size = buckets_size[i],
		        )
		        if not Bucket.objects.filter(bucket_name=buckets_list).exists():
		            bucket.save()




	def save_ec2_instances(self):
		# Store EC2 instances  in db
		Reservations, ResponseMetadata = AWS_EC2.connect_ec2()
		EC2_ins = EC2(
		            Reservations = Reservations,
		            ResponseMetadata = ResponseMetadata,

		)

		DB_Recordscount = EC2.objects.all().count()
		if DB_Recordscount ==0 :
			EC2_ins.save()