from rest_framework import serializers
from .models import Bucket, EC2




class BucketSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Bucket
        fields = '__all__'



class EC2Serializer(serializers.ModelSerializer):
 
    class Meta:
        model = EC2
        fields = '__all__'