from django.db import models




class Bucket(models.Model):
	bucket_name = models.CharField(max_length=90000)
	access_permissions = models.CharField(max_length=90000)
	bucket_privacy = models.CharField(max_length=90000)
	bucket_size = models.CharField(max_length=90000)

	class Meta:
		verbose_name_plural = 'Buckets'




class EC2(models.Model):
	Reservations = models.CharField(max_length=90000)
	ResponseMetadata = models.CharField(max_length=90000)

	class Meta:
		verbose_name_plural = 'EC2 Instances'