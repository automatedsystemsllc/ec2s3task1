from rest_framework.routers import SimpleRouter
from .views import BucketViewset, EC2Viewset




router = SimpleRouter()
router.register('ec2', EC2Viewset)
router.register('buckets', BucketViewset)
urlpatterns = router.urls



